var express = require('express'),
  app = express(),
  port = process.env.PORT || 3002;

var path = require('path');
app.use(express.static(__dirname + '/build'));

app.listen(port);

console.log('Aplicacion CotizadorWeb desde Node: ' + port);

app.get("/", function(req, res){
  res.sendFile('index.html',{root:'.'});
});