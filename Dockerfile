# Imagen base
FROM node:latest


# Directorio de la app
WORKDIR /app

#Copiado de archivos
ADD /dist /app/build
ADD /deploy-utils/server.js /app
ADD /deploy-utils/package.json /app


# Dependencias
RUN npm install

# Puerto que expongo
EXPOSE 3002

#Comando
CMD ["npm", "start"]
