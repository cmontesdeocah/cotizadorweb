import './styles.css';
import './views/ctz-base';
import './views/ctz-login';
import './views/ctz-new-cotizador';
import './views/ctz-tray-evaluation';
import './views/ctz-evaluation';
import './views/ctz-bandeja';

import '@vaadin/vaadin-text-field/vaadin-text-field';
import '@vaadin/vaadin-text-field/vaadin-password-field';
import '@vaadin/vaadin-text-field/vaadin-email-field';
import '@vaadin/vaadin-button';
import '@vaadin/vaadin-checkbox';
import '@vaadin/vaadin-radio-button/vaadin-radio-button';
import '@vaadin/vaadin-radio-button/vaadin-radio-group';
import '@vaadin/vaadin-combo-box/vaadin-combo-box.js';
import '@vaadin/vaadin-custom-field/vaadin-custom-field.js';
import '@vaadin/vaadin-date-picker/vaadin-date-picker.js';
import '@vaadin/vaadin-icons/vaadin-icons.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@vaadin/vaadin-grid/vaadin-grid-selection-column.js';

import { Router } from '@vaadin/router';

window.addEventListener('load', () => { 
    initRouter();
  });
  
  function initRouter() {
    const router = new Router(document.querySelector('main')); 
    router.setRoutes([
      {
        path: '/',
        component: 'ctz-login'
      },
      {
        path: '/cotizador',
        component: 'ctz-new-cotizador',
        action: () =>
          import('./views/ctz-new-cotizador') 
      },
      {
        path: '/bandeja',
        component: 'ctz-bandeja',
        action: () =>
          import('./views/ctz-bandeja') 
      },
      {
        path: '/evaluacion',
        component: 'ctz-evaluation',
        action: () =>
          import('./views/ctz-evaluation') 
      },                  
      {
        path: '(.*)', 
        component: 'ctz-not-found',
        action: () =>
          import('./views/ctz-not-found')
      },
      
    ]);
  }