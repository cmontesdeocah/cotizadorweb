import nanoid from 'nanoid';
export const UPDATE_LOGIN = 'UPDATE_LOGIN';
export const UPDATE_PERSONA = 'UPDATE_PERSONA';
export const UPDATE_TEA = 'UPDATE_TEA';
export const LOGOUT = 'LOGOUT';
export const ADD_TODO = 'ADD_TODO';
export const UPDATE_CLIENTE = 'UPDATE_CLIENTE';
export const UPDATE_COTIZACION = 'UPDATE_COTIZACION';
export const UPDATE_BUSQUEDA_COTIZACIONES = 'UPDATE_BUSQUEDA_COTIZACIONES';
export const SELECION_ITEM = 'SELECION_ITEM';

export const addTodo = task => {
  return {
    type: ADD_TODO,
    todo: { 
      id: nanoid(),
      task,
      complete: false
    }
  };
};

export const updateLogin = user => {
  return {
    type: UPDATE_LOGIN,
    user : user
  };
};

export const logoutUser = user => {
  return {
    type: LOGOUT,
    user : user
  };
};

export const updateCliente = cliente => {
  return {
    type: UPDATE_CLIENTE,
    user : cliente
  };
};

export const updatePersona = persona => {
  return {
    type: UPDATE_PERSONA,
    persona : persona
  };
};

export const updateTea = tea => {
  return {
    type: UPDATE_TEA,
    tea : tea
  };
};

export const updateCotizacion = cotizacion => {
  return {
    type: UPDATE_COTIZACION,
    cotizacion : cotizacion
  };
};

export const updateBusquedaCotizaciones = cotizaciones => {
  return {
    type: UPDATE_BUSQUEDA_COTIZACIONES,
    cotizaciones : cotizaciones
  };
};

export const seleccionarItem = itemSeleccion => {
  return {
    type: SELECION_ITEM,
    itemSeleccion : itemSeleccion
  };
};
