import { ADD_TODO, UPDATE_LOGIN, UPDATE_CLIENTE, LOGOUT, UPDATE_PERSONA, 
        UPDATE_TEA, UPDATE_COTIZACION, UPDATE_BUSQUEDA_COTIZACIONES, SELECION_ITEM } from './actions.js';
import { URLSearchParams } from 'url';

const APP_STATE = {
    user: {nombre:String,email:String,token:String},
    cliente: {type:Object},
    persona:{tipoDocumento:String,numDocumento:String,codCentral:String,nombre:String,buro:String,fecAlta:String,nivelRiesgo:String,
            vinculacion:String,segmento:{codSegmento:String,nomSegmento:String},oficina:{codOficina:String,nomOficina:String},
            gestor:{codGestor:String,nomGestor:String}},
    tea:{teaMinima:String,teaSugerida:String,teaSolicitada:String},
    cotizacion:{producto:{tipoProducto:String,nomProducto:String},importeOperacion:{moneda:String,monto:String},
                plazo:{tipoPeriodo:String,periodo:String},modalidad:String,garantia:String,teaSugerida:String,
                teaMinima:String,teaSolicitada:String,comentarios:String,estado:String},    
    todos: [],
    cotizaciones: [],
    itemSeleccion : {type:Object}
};

export const reducer = (state = APP_STATE, action) => {
    switch (action.type) {
        case UPDATE_LOGIN:
            return {
                ...state,
                user: action.user
            };
        case UPDATE_CLIENTE:
            return {
                ...state,
                cliente: action.cliente
            };            
        case ADD_TODO:
            return {
                ...state,
                todos: [...state.todos, action.todo]
            };
        case UPDATE_PERSONA:
            return {
                ...state,
                persona: action.persona
            };
        case UPDATE_TEA:
            return {
                ...state,
                tea: action.tea
            };
        case UPDATE_COTIZACION:
            return {
                ...state,
                cotizacion: action.cotizacion
            }; 
        case UPDATE_BUSQUEDA_COTIZACIONES:
            return {
                ...state,
                cotizaciones: action.cotizaciones
            };  
        case SELECION_ITEM:
            return {
                ...state,
                itemSeleccion: action.itemSeleccion
            };                      
        case LOGOUT:
            state=undefined;
            return state;                  
        default:
            return state;
    }
};