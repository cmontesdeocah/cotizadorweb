import { LitElement, html } from 'lit-element';
import { connect } from 'pwa-helpers';
import { store } from '../redux/store.js';
import { addTodo } from '../redux/actions.js';
import '../redux/reducer.js';

import '../components/ctz-header';
import '../components/ctz-contenedor-principal';
import '../components/ctz-datos-cliente';
import '../components/ctz-datos-financieros';
import '../components/ctz-datos-producto';
import '../components/ctz-calculadora-prestamo';


class CtzNewCotizador extends connect(store)(LitElement) { 

  static get properties() { 
    return {
      todos: { type: Array },
      task: { type: String }
    };
  }

  constructor() { 
    super();
    this.task = '';
  }

  stateChanged(state){
    
  }

  addTodo() {
    if (this.task) {
      store.dispatch(addTodo(this.task));
      this.task = '';
    }
  }

  shortcutListener(e) {
    if (e.key === 'Enter') { 
      this.addTodo();
    }
  }

  updateTask(e){
    this.task = e.target.value;
  }

  render() {
    return html` 
    <style>
      .contenedorCotizador {
          display: grid;
          
          grid-template-columns: 1fr 1fr ;
          grid-template-rows: auto;
          grid-template-areas: "datos1 datos2";
                                  
          column-gap: 5px;
          row-gap: 0px;
          margin-bottom: 0px;
          width: 450px;
          padding: 0px;
      }
      .datos1 {
          grid-area: datos1;
          display: flex;
          flex-direction:column;
          align-items: flex-start;
          margin-right:20px;
      } 
      .datos2 {
          grid-area: datos2;
          flex-direction:column;
          align-items: flex-start;
      } 
      @media only screen and (max-width: 920px) {
          .contenedorCotizador {
            display: grid;
            
            grid-template-columns: 1fr ;
            grid-template-rows: auto;
            grid-template-areas: "datos1"
                                "datos2";
            column-gap: 5px;
            row-gap: 0px;
            margin-bottom: 0px;
            width: 100%;
            padding: 0px;
        }  
        .datos1 {
            grid-area: datos1;
            display: flex;
            flex-direction:column;
            align-items: center;
        } 
        .datos2 {
            grid-area: datos2;
            display: flex;
            margin-top:20px;
            margin-right:20px;
            flex-direction:column;
            align-items: center;
        }               
      }  
    
    </style>

      <ctz-header view="cotizador"></ctz-header>
      <ctz-contenedor-principal>
        <div class="contenedorCotizador"> 
          <div class="datos1">
            <ctz-datos-cliente accion="nuevo"></ctz-datos-cliente>
            <ctz-datos-financieros accion="nuevo"></ctz-datos-financieros>
          </div>
          <div class="datos2">
            <ctz-datos-producto accion="nuevo"></ctz-datos-producto>
            <ctz-calculadora-prestamo accion="nuevo"></ctz-calculadora-prestamo>
          </div>    
        </div>  
      </ctz-contenedor-principal>    
    `;
  }
}

customElements.define('ctz-new-cotizador', CtzNewCotizador);