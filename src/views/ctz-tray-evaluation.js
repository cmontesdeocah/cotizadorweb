import { LitElement, html } from 'lit-element';
import '../components/ctz-header';
import '../components/ctz-contenedor-principal';

class CtzTrayEvaluation extends LitElement {
  render() {
    return html`
    <ctz-header></ctz-header>
    <ctz-contenedor-principal>
      <h3>View Tray Evaluation!</h3>
      <p>
        Please development Tray Evaluation.
      </p>
    </ctz-contenedor-principal>
    `;
  }
}

customElements.define('ctz-tray-evaluation', CtzTrayEvaluation);