import { html } from 'lit-element';
import { CtzBase } from './ctz-base';

class CtzNotFoundView extends CtzBase {
  render() {
    return html`
      <h3>View not found!</h3>
      <p>
        Please check your URL.
      </p>
    `;
  }
}

customElements.define('ctz-not-found', CtzNotFoundView);