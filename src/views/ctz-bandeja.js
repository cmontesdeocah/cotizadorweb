import { LitElement, html } from 'lit-element';
import '../components/ctz-header';
import '../components/ctz-contenedor-principal';
import '../components/ctz-filtro-busqueda';
import '../components/ctz-parrilla';
import { connect } from 'pwa-helpers';
import { store } from '../redux/store.js';
import '../redux/reducer.js';

class CTZBandeja extends connect(store)(LitElement) {

    static get properties() { 
        return {
            cotizaciones: { type: Array },
        };
    }

    stateChanged(state){
        this.cotizaciones = state.cotizaciones;
    }    

    constructor(){
        super();   
    }

    connectedCallback() {
        super.connectedCallback();
        console.log("entro al dom");
        //console.log(this.shadowRoot.childNodes[2].shadowRoot.children[2].innerHTML);

    }

    firstRendered() {
        console.log("entro al ren");
        //console.log(this.shadowRoot.getElementById('grillita'));
    }

    render(){
        return html`
            <ctz-header view="bandeja"></ctz-header>
            <ctz-contenedor-principal>
                <ctz-filtro-busqueda></ctz-filtro-busqueda>
                <br>
                <ctz-parrilla></ctz-parrilla>
            </ctz-contenedor-principal>
        `;
    }
}

customElements.define('ctz-bandeja',CTZBandeja);