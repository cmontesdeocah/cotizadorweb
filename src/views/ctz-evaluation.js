import { LitElement, html } from 'lit-element';
import { connect } from 'pwa-helpers';
import { store } from '../redux/store.js';
import { updatePersona, updateTea, updateCotizacion } from '../redux/actions.js';
import '../redux/reducer.js';
import axios from 'axios';
import * as Config from '../config/variable_config';

import '../components/ctz-header';
import '../components/ctz-contenedor-principal';
import '../components/ctz-datos-cliente';
import '../components/ctz-datos-financieros';
import '../components/ctz-datos-producto';
import '../components/ctz-calculadora-prestamo';

class CtzEvaluation extends connect(store)(LitElement) {

  static get properties() { 
    return {
      item: { type: Object },
      usuario:  {'nombre':String,'email':String,'token':String},
      tipoDocumento: {type:String},
      numDocumento: {type:String},
      cotizacion:{producto:{tipoProducto:String,nomProducto:String},importeOperacion:{moneda:String,monto:String},
                  plazo:{tipoPeriodo:String,periodo:String},modalidad:String,garantia:String,teaSugerida:String,
                  teaMinima:String,teaSolicitada:String,comentarios:String,estado:String},
      tea:{teaMinima:String,teaSugerida:String,teaSolicitada:String}
    };
  }

  constructor() { 
    super();
  }

  stateChanged(state){
    this.item=state.itemSeleccion;
    this.usuario = state.user;
    this.cotizacion = state.cotizacion;

  }

  connectedCallback() {
    super.connectedCallback();
    this.cargarDatosEditar();

  }

  cargarDatosEditar(){
    this.obtenerDatosCotizacion();
    this.obtenerPersona();
  }

  obtenerPersona(){
    console.log("Ingreso obtener Persona");
    this.tipoDocumento = this.item.documento.split("-")[0];
    this.numDocumento = this.item.documento.split("-")[1];

    const reqHeaderToken = {
        headers: {'Authorization': 'JWT ' + this.usuario.token}
    };
    axios.get(Config.URL_BACK + '/v1/personas/tipoDocumento/'+ this.tipoDocumento+'/numDocumento/'+this.numDocumento, reqHeaderToken)
    .then(response => { 
        console.log("Obteniendo respuesta Persona JSON");
        console.log(response.data); 
        store.dispatch(updatePersona({'tipoDocumento':this.tipoDocumento,'numDocumento':this.numDocumento,'codCentral':response.data.data.codCentral,
                                    'nombre':response.data.data.nombre,'buro':response.data.data.buro,'fecAlta':response.data.data.fecAlta,
                                    'nivelRiesgo':response.data.data.nivelRiesgo,'vinculacion':response.data.data.vinculacion,'segmento':response.data.data.segmento,
                                    'oficina':response.data.data.oficina,'gestor':response.data.data.gestor}));
          
    }).catch(e => {
        //this.resetPersona();
        console.log(e);
    });          
  }
  
  obtenerDatosCotizacion(){
    
    this.cotizacion.importeOperacion.moneda=this.item.moneda;
    this.cotizacion.importeOperacion.monto=this.item.importe;
    this.cotizacion.plazo.periodo=this.item.plazo;
    this.cotizacion.producto.nomProducto="PLD";
    this.cotizacion.producto.tipoProducto="PLD";
    this.cotizacion.plazo.tipoPeriodo=this.item.unidad;
    this.cotizacion.garantia="Sin Garantia";
    this.cotizacion.modalidad="Nuevo";
    store.dispatch(updateCotizacion(this.cotizacion));
  }

  render() {
    return html` 
    <style>
      .contenedorCotizador {
          display: grid;
          
          grid-template-columns: 1fr 1fr ;
          grid-template-rows: auto;
          grid-template-areas: "datos1 datos2";
                                  
          column-gap: 5px;
          row-gap: 0px;
          margin-bottom: 0px;
          width: 450px;
          padding: 0px;
      }
      .datos1 {
          grid-area: datos1;
          display: flex;
          flex-direction:column;
          align-items: flex-start;
          margin-right:20px;
      } 
      .datos2 {
          grid-area: datos2;
          flex-direction:column;
          align-items: flex-start;
      } 
      @media only screen and (max-width: 920px) {
          .contenedorCotizador {
            display: grid;
            
            grid-template-columns: 1fr ;
            grid-template-rows: auto;
            grid-template-areas: "datos1"
                                "datos2";
            column-gap: 5px;
            row-gap: 0px;
            margin-bottom: 0px;
            width: 100%;
            padding: 0px;
        }  
        .datos1 {
            grid-area: datos1;
            display: flex;
            flex-direction:column;
            align-items: center;
        } 
        .datos2 {
            grid-area: datos2;
            display: flex;
            margin-top:20px;
            margin-right:20px;
            flex-direction:column;
            align-items: center;
        }               
      }  
    
    </style>

    <ctz-header view="evaluacion"></ctz-header>
      <ctz-contenedor-principal>
        <div class="contenedorCotizador"> 
          <div class="datos1">
            <ctz-datos-cliente accion="modificado"></ctz-datos-cliente>
            <ctz-datos-financieros accion="modificado"></ctz-datos-financieros>
          </div>
          <div class="datos2">
            <ctz-datos-producto accion="modificado"></ctz-datos-producto>
            <ctz-calculadora-prestamo accion="modificado" estado="${this.item.estado}" teaAprob="${this.item.teaprob}"></ctz-calculadora-prestamo>
          </div>    
        </div>  
      </ctz-contenedor-principal>    
    `;
  }
}

customElements.define('ctz-evaluation', CtzEvaluation);