//import { html } from 'lit-element';
//import { CtzBase } from './ctz-base';
import { LitElement, html } from 'lit-element';
import { connect } from 'pwa-helpers';
import { store } from '../redux/store.js';
import { updatePersona, updateTea } from '../redux/actions.js';
import '../redux/reducer.js';
import { Router } from '@vaadin/router';
import * as Config from '../config/variable_config';
import axios from 'axios';

class CtzCotizador extends connect(store)(LitElement) {

    static get properties() { 
        return {
            persona:{tipoDocumento:String,numDocumento:String,codCentral:String,nombre:String,buro:String,fecAlta:String,nivelRiesgo:String,
                   vinculacion:String,segmento:{codSegmento:String,nomSegmento:String},oficina:{codOficina:String,nomOficina:String},
                   gestor:{codGestor:String,nomGestor:String}},
            user: { type:String },
            password: { type:String },
            tipoDocumento: { type:String },
            numDocumento: { type:String },
            cotizacion:{producto:{tipoProducto:String,nomProducto:String},importeOperacion:{moneda:String,monto:String},
                      plazo:{tipoPeriodo:String,periodo:String},modalidad:String,garantia:String,teaSugerida:String,
                      teaMinima:String,teaSolicitada:String,comentarios:String,estado:String},
            tea:{teaMinima:String,teaSugerida:String, teaSolicitada:String}
        };
      }
    
    constructor() { 
        super();
        this.user="carlos.montesdeoca.h@gmail.com";
        this.password="superstrongpassword";
        this.tipoDocumento="DNI";
        this.numDocumento="";
        this.cotizacion={producto:{tipoProducto:"PLD",nomProducto:"PRESTAMO LIBRE DISPONIBILIDAD"},importeOperacion:{moneda:"PEN",monto:""},
        plazo:{tipoPeriodo:"MES",periodo:""},modalidad:"NUEVO",garantia:"SIN GARANTIA",teaSugerida:"",
        teaMinima:"",teaSolicitada:"",comentarios:"",estado:""}
        console.log("Ingreso al constructor cotizador.js");
    }

    stateChanged(state){
        this.persona = state.persona;
        this.tea = state.tea;
        console.log(this.persona);
    }

    updateNumDocumento(e){
        this.numDocumento = e.target.value;
    }

    updateImporte(e){
        this.cotizacion.importeOperacion.monto=e.target.value;
    }

    updatePlazo(e){
        this.cotizacion.plazo.periodo=e.target.value;
    }

    updateTeaSolicitada(e){
        this.cotizacion.teaSolicitada=e.target.value;
    }

    resetPersona(){
        store.dispatch(updatePersona({tipoDocumento:"",numDocumento:"",codCentral:"",nombre:"",buro:"",fecAlta:"",nivelRiesgo:"",
                vinculacion:"",segmento:{codSegmento:"",nomSegmento:""},oficina:{codOficina:"",nomOficina:""},
                gestor:{codGestor:"",nomGestor:""}}));
        store.dispatch(updateTea({teMinima:"",teaSugerida:"",teaSolicitada:""}));
    }

    obtenerPersona(){
        console.log("Ingreso obtener Persona");
        let reqHeader = { 
            headers: {'Content-Type':'application/json'} 
        };
        axios.post(Config.URL_BACK + '/v1/auth/', {
            email: this.user,
            password:this.password
        }, reqHeader).then(responseToken => {
            const reqHeaderToken = {
                headers: {'Authorization': 'JWT ' + responseToken.data.data.token}
            };
            axios.get(Config.URL_BACK + '/v1/personas/tipoDocumento/'+ this.tipoDocumento+'/numDocumento/'+this.numDocumento, reqHeaderToken)
            .then(response => { 
                console.log("Obteniendo respuesta Persona JSON");
                console.log(response.data); 
                store.dispatch(updatePersona({'tipoDocumento':this.tipoDocumento,'numDocumento':this.numDocumento,'codCentral':response.data.data.codCentral,
                                            'nombre':response.data.data.nombre,'buro':response.data.data.buro,'fecAlta':response.data.data.fecAlta,
                                            'nivelRiesgo':"",'vinculacion':response.data.data.vinculacion,'segmento':response.data.data.segmento,
                                            'oficina':response.data.data.oficina,'gestor':response.data.data.gestor}));
                Router.go('/cotizador');  
            }).catch(e => {
                this.resetPersona();
                console.log(e);
            }); 
        }).catch(e => {
            console.log(e);
        });         
    }

    calcularTasaModelo(){
        console.log("Ingreso calcularTasaModelo");
        let reqHeader = { 
            headers: {'Content-Type':'application/json'} 
        };
        axios.get('http://www.mocky.io/v2/5ce0d32c32000087002f624d',reqHeader)
            .then(response => { 
                console.log("Obteniendo respuesta Mocky");
                console.log(response.data);
                var teaMinima=parseFloat(response.data.data[0].effectiveRate.detailedEffectiveRates[0].percentage)*100;
                var teasugerida=parseFloat(response.data.data[0].effectiveRate.detailedEffectiveRates[1].percentage)*100;
                store.dispatch(updateTea({'teaMinima':teaMinima,'teaSugerida':teasugerida,'teaSolicitada':teasugerida}));
                Router.go('/cotizador');  
            }).catch(e => {
                console.log(e);
            });
    }

    registrarCotizacion(){
        console.log("Ingreso registrarCotizacion");
        console.log(this.cotizacion);
        let reqHeader = { 
            headers: {'Content-Type':'application/json'} 
        };
        axios.post(Config.URL_BACK + '/v1/auth/', {
            email: this.user,
            password:this.password
        }, reqHeader).then(responseToken => {
            const reqHeaderToken = {
                headers: {'Authorization': 'JWT ' + responseToken.data.data.token}
            };
            axios.post(Config.URL_BACK + '/v1/cotizaciones', {
                codPersona:this.persona.codCentral,
                documento:{tipoDocumento:this.tipoDocumento,numDocumento:this.numDocumento},
                producto:{tipoProducto:this.cotizacion.producto.tipoProducto,nomProducto:this.cotizacion.producto.nomProducto},
                importeOperacion:{moneda:this.cotizacion.importeOperacion.moneda,monto:this.cotizacion.importeOperacion.monto},
                plazo:{tipoPeriodo:this.cotizacion.plazo.tipoPeriodo,periodo:this.cotizacion.plazo.periodo},
                modalidad:this.cotizacion.modalidad,
                garantia:this.cotizacion.garantia,
                teaSugerida:this.tea.teaSugerida,
                teaMinima:this.tea.teaMinima,
                teaSolicitada:this.tea.teaSolicitada,
                comentarios:this.cotizacion.comentarios,
                estado:this.cotizacion.estado
            },reqHeaderToken)
            .then(response => { 
                console.log("Obteniendo respuesta POST cotizacion");
                console.log(response.data); 
                Router.go('/cotizador');  
            }).catch(e => {
                console.log(e);
            }); 
        }).catch(e => {
            console.log(e);
        });
    }

    render() {
        return html`
         <ctz-header view="cotizador"></ctz-header>
            <style>
                .grillaLogin {
                    display: grid;
                    
                    grid-template-columns: 1fr 1fr 1fr 1fr;
                    grid-template-rows: auto;
                    grid-template-areas: "txt-codcentral txt-tipodoc txt-nrodoc boton-login"
                                         "txt-nomcliente txt-nomcliente txt-nomcliente txt-nomcliente"
                                         "txt-codsegmento txt-segmento txt-buro txt-fecalta"
                                         "txt-nivelriesgo txt-nivelriesgo . ."
                                         "txt-codoficina txt-nomoficina txt-codgestor txt-nomgestor"
                                         "txt-vincula txt-vincula . ."
                                         "cbx-producto cbx-producto cbx-subprod cbx-modal"
                                         "lbl-importe cbx-moneda txt-importe cbx-garantia"
                                         "lbl-plazo cbx-uniplazo txt-plazo .";
                    grid-gap: 10px;
                    padding: 1%;
                    /*margin-bottom: 25px;*/
                    width: 50%; 
                   float: left;
                }
                .row:after {
                    content: "";
                    display: table;
                    clear: both;
                }

                .panel_tasa {
                    display: grid;
                    
                    grid-template-columns: 1fr 1fr 1fr;
                    grid-template-rows: auto;
                    grid-template-areas: "txt-teasugerida txt-teaminima ."
                                         "txt-teasoli  boton-calcular  ."
                                         "txt-sustento txt-sustento txt-sustento"
                                         "boton-cerrar boton-cerrar  ."
                                         "boton-cerrar boton-cerrar  .";
                    grid-gap: 10px;
                    padding: 1%;
                    width: 45%; 
                    float:right;
                }
                .grillaLogin_item21{
                    grid-area: cbx-producto;
                }
                .grillaLogin_item22{
                    grid-area: cbx-subprod;
                }
                .grillaLogin_item23{
                    grid-area: cbx-modal;
                }
                .panel_tasa_item01{
                    grid-area: txt-teasugerida;
                }
                .panel_tasa_item02{
                    grid-area: txt-teaminima;
                }
                .panel_tasa_item03{
                    grid-area: btn-calcular;
                    text-align: center;
                    align-items: baseline;
                    float: left;
                }
                .panel_tasa_item04 {
                    grid-area: txt-teasoli;
                }
                .panel_tasa_item05 {
                    grid-area: txt-sustento;
                    max-height: 150px;
                }
                .panel_tasa_item06{
                    grid-area: btn-cerrar;
                    float: right;
                    max-height: 150px;
                }
                .grillaLogin_item1 {
                    grid-area: lbl-codcentral;
                    display: flex;
                    align-items: flex-start;
                }
                .grillaLogin_item2 {
                    grid-area: txt-codcentral;
                    align-items: flex-end;
                }
                .grillaLogin_item3 {
                    grid-area: lbl-tipodoc;
                    display: flex;
                    align-items: center;
                }
                .grillaLogin_item4 {
                    grid-area: txt-tipodoc;
                }
                .grillaLogin_item5 {
                    grid-area: boton-login;
                    text-align: center;
                    align-items: baseline;
                }
                .grillaLogin_item6 {
                    grid-area: lbl-nrodoc;
                    display: flex;
                    align-items: center;
                }
                .grillaLogin_item7 {
                    grid-area: txt-nrodoc;
                }
                .grillaLogin_item8 {
                    grid-area: lbl-nomcliente;
                    display: flex;
                    align-items: center;
                }
                .grillaLogin_item9 {
                    grid-area: txt-nomcliente;
                    /*position: relative;
	                padding: 0.65rem;
                    float: left;
                    box-sizing: border-box;
                    width:100%;
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;*/
                }
                .grillaLogin_item10 {
                    grid-area: txt-codsegmento;
                } 
                .grillaLogin_item11 {
                    grid-area: txt-segmento;
                }  
                .grillaLogin_item12 {
                    grid-area: txt-buro;
                }         
                .grillaLogin_item13 {
                    grid-area: txt-fecalta;
                } 
                .grillaLogin_item14 {
                    grid-area: txt-nivelriesgo;
                }    
                .grillaLogin_item15 {
                    grid-area: txt-codoficina;
                }    
                .grillaLogin_item16 {
                    grid-area: txt-nomoficina;
                } 
                .grillaLogin_item17 {
                    grid-area: txt-codgestor;
                }   
                .grillaLogin_item18 {
                    grid-area: txt-nomgestor;
                }     
                .grillaLogin_item19 {
                    grid-area: txt-vincula;
                }     
                .grillaLogin_item25 {
                    grid-area: txt-vincula;
                }
                .grillaLogin_item26 {
                    grid-area: lbl-importe;
                }
                .grillaLogin_item27 {
                    grid-area: cbx-moneda;
                }
                .grillaLogin_item28 {
                    grid-area: txt-importe;
                }
                .grillaLogin_item29 {
                    grid-area: lbl-garantia;
                }
                .grillaLogin_item30 {
                    grid-area: cbx-garantia;
                } 
                .grillaLogin_item31 {
                    grid-area: lbl-plazo;
                } 
                .grillaLogin_item32 {
                    grid-area: cbx-uniplazo;
                }   
                .grillaLogin_item33 {
                    grid-area: txt-plazo;
                }                     
            </style>
<div class="row">
            <div id="contenedorLogin" class="grillaLogin">
                <vaadin-tab class="grillaLogin_item2">
                    <div>Código Central</div>
                    <div>
                        <vaadin-text-field placeholder="Código Central" value=${this.persona.codCentral}> 
                        </vaadin-text-field>
                    </div>
                </vaadin-tab>
                <vaadin-tab class="grillaLogin_item4">
                    <div>Tipo Documento</div>
                    <div>
                        <vaadin-combo-box placeholder="DNI"></vaadin-combo-box> 
                    </div>  
                </vaadin-tab>
                <vaadin-tab class="grillaLogin_item7">
                    <div> Nro. Documento</div>
                    <div>
                        <vaadin-text-field placeholder="Nro. Documento" value=${this.numDocumento} @change="${this.updateNumDocumento}"> 
                        </vaadin-text-field>
                    </div>
                </vaadin-tab>
                <vaadin-tab class="grillaLogin_item5">
                    <div>          
                        <vaadin-button theme="primary" @click="${this.obtenerPersona}">Buscar</vaadin-button>
                    </div>
                </vaadin-tab>
                <vaadin-tab class="grillaLogin_item9">
                    <div>Nombre / Razón Social de Cliente</div>
                    <div>
                        <vaadin-text-field placeholder="Nombre cliente" value=${this.persona.nombre}> 
                        </vaadin-text-field>
                    </div>
                </vaadin-tab>
                <vaadin-tab class="grillaLogin_item10">
                    <div>Segmento:</div>
                    <div>
                        <vaadin-text-field placeholder="" value=${this.persona.segmento.codSegmento}> 
                        </vaadin-text-field>
                    </div>
                </vaadin-tab>
                <vaadin-tab class="grillaLogin_item11">
                    <div>-</div>
                    <div>
                        <vaadin-text-field placeholder="" value=${this.persona.segmento.nomSegmento}> 
                        </vaadin-text-field>
                    </div>
                </vaadin-tab>
                <vaadin-tab class="grillaLogin_item12">
                    <div>Buró:</div>
                    <div>
                        <vaadin-text-field placeholder="" value=${this.persona.buro}> 
                        </vaadin-text-field>
                    </div>
                </vaadin-tab>
                <vaadin-tab class="grillaLogin_item13">
                    <div>Fecha de Alta:</div>
                    <div>
                        <vaadin-text-field placeholder="" value=${this.persona.fecAlta}> 
                        </vaadin-text-field>
                    </div>
                </vaadin-tab>
                <vaadin-tab class="grillaLogin_item14">
                    <div>Nivel Riesgo:</div>
                    <div>
                        <vaadin-text-field placeholder="" value=${this.persona.nivelRiesgo}> 
                        </vaadin-text-field>
                    </div>
                </vaadin-tab>
                <vaadin-tab class="grillaLogin_item15">
                    <div>Oficina Principal:</div>
                    <div>
                        <vaadin-text-field placeholder="" value=${this.persona.oficina.codOficina}> 
                        </vaadin-text-field>
                    </div>
                </vaadin-tab>
                <vaadin-tab class="grillaLogin_item16">
                    <div>-</div>
                    <div>
                        <vaadin-text-field placeholder="" value=${this.persona.oficina.nomOficina}> 
                        </vaadin-text-field>
                    </div>
                </vaadin-tab>
                <vaadin-tab class="grillaLogin_item17">
                    <div>Gestor Principal:</div>
                    <div>
                        <vaadin-text-field placeholder="" value=${this.persona.gestor.codGestor}> 
                        </vaadin-text-field>
                    </div>
                </vaadin-tab>
                <vaadin-tab class="grillaLogin_item18">
                    <div>-</div>
                    <div>
                        <vaadin-text-field placeholder="" value=${this.persona.gestor.nomGestor}> 
                        </vaadin-text-field>
                    </div>
                </vaadin-tab>
                <vaadin-tab class="grillaLogin_item19">
                    <div>Vinculación:</div>
                    <div>
                        <vaadin-text-field placeholder="" value=${this.persona.vinculacion}> 
                        </vaadin-text-field>
                    </div>
                </vaadin-tab>
                <vaadin-tab class="grillaLogin_item21">
                    <div>Producto:</div>
                    <div>
                        <vaadin-combo-box placeholder="PLD" value="XXX"></vaadin-combo-box> 
                    </div>  
                </vaadin-tab>
                <vaadin-tab class="grillaLogin_item22">
                    <div>Tipo de Producto:</div>
                    <div>
                        <vaadin-combo-box placeholder="PLD"></vaadin-combo-box> 
                    </div>  
                </vaadin-tab>
                <vaadin-tab class="grillaLogin_item23">
                    <div>Modalidad:</div>
                    <div>
                        <vaadin-combo-box placeholder="Nuevo"></vaadin-combo-box> 
                    </div>  
                </vaadin-tab>
                <div class="grillaLogin_item26">Importe de la Operación</div>
                <div class="grillaLogin_item27">
                        <vaadin-combo-box id="cbxmoneda" placeholder="PEN" label="" items = "['PEN', 'USD']"></vaadin-combo-box>
                        <script>
                            customElements.whenDefined('vaadin-combo-box').then(function() {
                            document.querySelector('#cbxmoneda').items = ['PEN', 'USD'];});
                        </script>
                </div>
                <div class="grillaLogin_item28">
                    <vaadin-text-field placeholder="" value="" @change="${this.updateImporte}"></vaadin-text-field>
                </div>

                <vaadin-tab class="grillaLogin_item30">
                    <div>Garantía:</div>
                    <div>
                        <vaadin-combo-box placeholder="SIN GARANTÍA"></vaadin-combo-box> 
                    </div>  
                </vaadin-tab>

                <div class="grillaLogin_item31">Plazo:</div>
                <div class="grillaLogin_item32">
                        <vaadin-combo-box placeholder="MES"></vaadin-combo-box> 
                </div>
                <div class="grillaLogin_item33">
                    <vaadin-text-field placeholder="" @change="${this.updatePlazo}"></vaadin-text-field>
                </div>
            </div> 
                
            <div class="panel_tasa">
                <vaadin-tab class="panel_tasa_item01">
                    <div>TEA Sugerida:</div>
                    <div>
                        <vaadin-text-field placeholder="" value=${this.tea.teaSugerida}> 
                        </vaadin-text-field>
                    </div>
                </vaadin-tab>
                <vaadin-tab class="panel_tasa_item02">
                    <div>TEA Mínima:</div>
                    <div>
                        <vaadin-text-field placeholder="" value=${this.tea.teaMinima}></vaadin-text-field>
                    </div>
                </vaadin-tab>
                <vaadin-tab class="panel_tasa_item03">
                    <div>
                        <vaadin-button theme="primary" @click="${this.calcularTasaModelo}">Calcular Tasa</vaadin-button>
                    </div>
                </vaadin-tab>
                <vaadin-tab class="panel_tasa_item04">
                    <div>TEA Solicitada:</div>
                    <div>
                        <vaadin-text-field placeholder="" value=${this.tea.teaSolicitada}> </vaadin-text-field>
                    </div>
                </vaadin-tab>
                <vaadin-tab class="panel_tasa_item05">
                    <div>Indicar sustento correspondiente:</div>
                    <div>
                        <vaadin-text-area label="ok" placeholder="Indique el sustento" disabled="false"></vaadin-text-area>
                    </div>
                </vaadin-tab>
                <vaadin-tab class="panel_tasa_item06">
                    <div>          
                        <vaadin-button theme="primary" @click="${this.registrarCotizacion}">Registrar</vaadin-button>
                    </div>
                </vaadin-tab>
            </div>
    </div>
        `;
  }
}

customElements.define('ctz-cotizador', CtzCotizador);