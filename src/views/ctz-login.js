import { LitElement, html } from 'lit-element';
import { connect } from 'pwa-helpers';
import { store } from '../redux/store.js';
import { updateLogin, updatePersona, updateTea } from '../redux/actions.js';
import '../redux/reducer.js';
import { Router } from '@vaadin/router';
import * as Config from '../config/variable_config';
import axios from 'axios';


class CtzLogin extends connect(store)(LitElement) {

    static get properties() { 
        return {
          usuario: {nombre:String,email:String,token:String},
          user: { type:String },
          password: { type:String }
        };
    }
    
    constructor() { 
        super();
        this.user="";
        this.password="";
    }

    stateChanged(state){
        this.usuario = state.user;
    }

    updateUser(e){
        this.user = e.target.value;
    }

    updatePassword(e){
        this.password = e.target.value;
    }

    iniciarSesion() {
        
        let reqHeader = { 
            headers: {'Content-Type':'application/json'} 
        };
        axios.post(Config.URL_BACK + '/v1/auth/', {
            email: this.user,
            password:this.password
        }, reqHeader).then(responseToken => {
            const reqHeaderToken = {
                headers: {'Authorization': 'JWT ' + responseToken.data.data.token}
            };
            axios.get(Config.URL_BACK + '/v1/users/email/'+ this.user, reqHeaderToken)
            .then(response => { 
                console.log(response.data.data[0]); 
                store.dispatch(updateLogin({'nombre':response.data.data[0].firstName + ' ' + response.data.data[0].lastName,
                                            'email':response.data.data[0].email,
                                            'token':responseToken.data.data.token}));
                store.dispatch(updatePersona({tipoDocumento:"",numDocumento:"",codCentral:"",nombre:"",buro:"",fecAlta:"",nivelRiesgo:"",
                vinculacion:"",segmento:{codSegmento:"",nomSegmento:""},oficina:{codOficina:"",nomOficina:""},
                gestor:{codGestor:"",nomGestor:""}}));
                store.dispatch(updateTea({teMinima:"",teaSugerida:"",teaSolicitada:""}));                   
                Router.go('/bandeja');  
            }).catch(e => {
                console.log(e);
            }); 
        }).catch(e => {
            console.log(e);
        });             
    }

    render() {
        return html`

            <style>
                .grillaLogin {
                    display: grid;
                    
                    grid-template-columns: 1fr 1fr;
                    grid-template-rows: auto;
                    grid-template-areas: "lbl-user  txt-user"
                                         "lbl-pass  txt-pass"
                                         "boton-login boton-login";
                    grid-gap: 20px;
                    margin-bottom: 25px;
                    width: 20%;
                    padding: 10%;
                }
                .grillaLogin_item1 {
                    grid-area: lbl-user;
                    display: flex;
                    align-items: center;
                    font-weight:500;
                }
                .grillaLogin_item2 {
                    grid-area: txt-user;
                }
                .grillaLogin_item3 {
                    grid-area: lbl-pass;
                    display: flex;
                    align-items: center;
                    font-weight:500;
                }
                .grillaLogin_item4 {
                    grid-area: txt-pass;
                }
                .grillaLogin_item5 {
                    grid-area: boton-login;
                    text-align: center;
                }

                vaadin-email-field {
                    font-size: 12px;
                    width: 220px;
                }

                vaadin-password-field {
                    font-size: 12px;
                    width: 220px;
                }
                
            </style>

            <div id="contenedorLogin" class="grillaLogin">
                <div class="grillaLogin_item1">
                   Usuario 
                </div>
                <div class="grillaLogin_item2">
                    <vaadin-email-field id="txtEmail" class="custom-style" .value=${this.user} @change="${this.updateUser}"> 
                    </vaadin-email-field>
                </div>
                <div class="grillaLogin_item3">
                    Contraseña
                </div>
                <div class="grillaLogin_item4">
                    <vaadin-password-field id="txtPassword" class="custom-style" .value=${this.password} @change="${this.updatePassword}"> 
                    </vaadin-password-field >
                </div>
                <div class="grillaLogin_item5">          
                    <vaadin-button theme="primary" @click="${this.iniciarSesion}"> 
                    iniciar
                    </vaadin-button>
                </div>
            </div>
        `;
  }
}

customElements.define('ctz-login', CtzLogin);