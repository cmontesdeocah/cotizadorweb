import { LitElement, html } from 'lit-element';
import '../components/ctz-header';
import '../components/ctz-contenedor-principal';

class CtzNewQuote extends LitElement {
  render() {
    return html`
      <ctz-header view="quote"></ctz-header>
      <ctz-contenedor-principal>
        <h3>View New Quote!</h3>
        <p>
          Please development New Quote.
        </p>
      </ctz-contenedor-principal>
    `;
  }
}

customElements.define('ctz-new-quote', CtzNewQuote);