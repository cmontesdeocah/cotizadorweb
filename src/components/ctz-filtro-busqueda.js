import { LitElement, html } from 'lit-element';
import { connect } from 'pwa-helpers';
import { store } from '../redux/store.js';
import '../redux/reducer.js';
import { updateBusquedaCotizaciones } from '../redux/actions.js';
import * as Config from '../config/variable_config';
import axios from 'axios';

class CtzFiltroBusqueda extends connect(store)(LitElement) {

    static get properties() { 
        return {
            busqueda: { type: Array },
            usuario:  {'nombre':String,'email':String,'token':String},
            documentos: {type:Object},
            estados: {type:Object},
            tipDocumento: { type:String },
            numDocumento: { type:String },
            codCentral: { type:String },
            idCotizacion: { type:String },
            fechaInicio: { type:String },
            fechaFin: { type:String },
        };
    }

    constructor(){
        super();
        this.documentos = ['DNI', 'EXT'];
        this.numDocumento="";
        this.idCotizacion="";
        //this.listarCotizaciones();
    }

    stateChanged(state){
        this.usuario = state.user;
    }

    updateNumDocumento(e){
        this.numDocumento = e.target.value;
    }

    updateIdCotizacion(e){
        this.idCotizacion = e.target.value;
    }

    filtrarCotizaciones(){
        var filter='';
        if(this.idCotizacion!=undefined && this.idCotizacion!=""){
            filter='/idsol/'+this.idCotizacion+'/tipoDocumento/0/numDocumento/0';
        }else if(this.numDocumento!=undefined && this.numDocumento!=""){
            filter='/idsol/0/tipoDocumento/DNI/numDocumento/'+this.numDocumento;
        }
        console.log("Ingreso a filtrarCotizaciones.");
            const reqHeaderToken = {
                headers: {'Authorization': 'JWT ' + this.usuario.token}
            };
            axios.get(Config.URL_BACK + '/v1/cotizaciones' + filter, reqHeaderToken)
            .then(response => { 
                console.log("Obteniendo respuesta cotizaciones");
                console.log(response.data.data.length);
                this.busqueda = [];
        
                var data = response.data.data;
                for (var i = 0; i < data.length; i++) {
                    var registro1 = new Object();
                    registro1.id = data[i].IDSOL;
                    registro1.fecha = data[i].dateCreated;
                    registro1.documento = data[i].documento.tipoDocumento+"-"+data[i].documento.numDocumento;
                    registro1.nombre = data[i].nomPersona;
                    registro1.moneda = data[i].importeOperacion.moneda;
                    registro1.importe = data[i].importeOperacion.monto;  
                    registro1.teasol = data[i].teaSolicitada;
                    registro1.teaprob = data[i].teaMinima;
                    registro1.unidad = data[i].plazo.tipoPeriodo; 
                    registro1.plazo = data[i].plazo.periodo; 
                    registro1.estado = data[i].estado;                           
                    registro1.desembolso = "";
                    this.busqueda[i] = registro1;
                }
                store.dispatch(updateBusquedaCotizaciones(this.busqueda));
            });
    }
    
    listarCotizaciones(){
        console.log("Ingreso a listarCotizaciones.");

            const reqHeaderToken = {
                headers: {'Authorization': 'JWT ' + this.usuario.token}
            };
            axios.get(Config.URL_BACK + '/v1/cotizaciones', reqHeaderToken)
            .then(response => { 
                console.log("Obteniendo respuesta cotizaciones");
                console.log(response.data.data.length);
                this.busqueda = [];
        
                var data = response.data.data;
                for (var i = 0; i < data.length; i++) {
                    var registro1 = new Object();
                    registro1.id = data[i].IDSOL;
                    registro1.fecha = data[i].dateCreated;
                    registro1.documento = data[i].documento.tipoDocumento+"-"+data[i].documento.numDocumento;
                    registro1.nombre = data[i].nomPersona;
                    registro1.moneda = data[i].importeOperacion.moneda;
                    registro1.importe = data[i].importeOperacion.monto;  
                    registro1.teasol = data[i].teaSolicitada;
                    registro1.teaprob = data[i].teaMinima;
                    registro1.unidad = data[i].plazo.tipoPeriodo; 
                    registro1.plazo = data[i].plazo.periodo; 
                    registro1.estado = data[i].estado;                           
                    registro1.desembolso = "";
                    this.busqueda[i] = registro1;
                }
                store.dispatch(updateBusquedaCotizaciones(this.busqueda));
            });
    }    

    render(){
        return html`
               <style>
                    .grilla-filtro {
                        display: grid;
                        
                        grid-template-columns: 1fr 1fr  1fr 1fr;
                        grid-template-rows: auto;
                        grid-template-areas: "cod-central  id-cotizacion  fecha-inicio  estado"
                                            "tipo-doc  nro-doc  fecha-fin  busqueda";
                        grid-gap: 15px;
                        margin-bottom: 25px;
                        width: 20%;

                    }               

                    .txtcodecentral {
                        grid-area: cod-central;
                        display: flex;
                        align-items: center;
                    }
                    .txtidcotizacion {
                        grid-area: id-cotizacion;
                    }
                    .dtfechainicio {
                        grid-area: fecha-inicio;
                    }
                    .cmbestado {
                        grid-area: estado;
                    }   
                    .cmbtipodoc {
                        grid-area: tipo-doc;
                    }
                    .nrodocumento {
                        grid-area: nro-doc;
                    }
                    .dtfechafin {
                        grid-area: fecha-fin;
                    }  
                    .btnbusqueda {
                        grid-area: busqueda;
                    } 

               @media only screen and (max-width: 768px) {
                    .grilla-filtro {
                        display: grid;
                        
                        grid-template-columns: 1fr  1fr 1fr;
                        grid-template-rows: auto;
                        grid-template-areas: "cod-central   id-cotizacion   estado"
                                            "tipo-doc   nro-doc  busqueda"
                                            "fecha-inicio   fecha-fin   busqueda";
                        grid-gap: 15px;
                        margin-bottom: 25px;
                        width: 20%;

                    }  
                    .btnbusqueda {
                        grid-area: busqueda;
                        display: flex;
                        flex-direction: column;
                        justify-content: center;
                        padding-left: 10px;
                    }                                
               }

               @media only screen and (max-width: 560px) {
                    .grilla-filtro {
                        display: grid;
                        
                        grid-template-columns: 1fr  1fr 1fr;
                        grid-template-rows: auto;
                        grid-template-areas: "cod-central   id-cotizacion   busqueda"
                                            "tipo-doc   nro-doc  busqueda"
                                            "fecha-inicio   fecha-fin   busqueda";
                        grid-gap: 15px;
                        margin-bottom: 25px;
                        width: 20%;

                    }  
                    .btnbusqueda {
                        grid-area: busqueda;
                        display: flex;
                        flex-direction: column;
                        justify-content: center;
                        padding-left: 10px;
                    }
                    .cmbestado {
                        display: none;
                    }                                                      
               }               
               </style>
               <div class="grilla-filtro">
                    <div  class="txtcodecentral">
                        <vaadin-text-field theme="small" placeholder="Codigo Central" value=""></vaadin-text-field>
                    </div>
                    <div class="txtidcotizacion">
                        <vaadin-text-field theme="small" placeholder="ID Cotizacion" value=${this.idCotizacion} @change="${this.updateIdCotizacion}"></vaadin-text-field>
                    </div>
                    <div class="cmbtipodoc">
                        <vaadin-combo-box theme="small" placeholder="Tipo de Doc" value=${this.tipoDocumento} items='${JSON.stringify(this.documentos)}'></vaadin-combo-box> 
                    </div>
                    <div class="nrodocumento">
                        <vaadin-text-field theme="small" placeholder="Nro Documento" value=${this.numDocumento} @change="${this.updateNumDocumento}"></vaadin-text-field>
                    </div>
                    <div class="dtfechainicio">
                          <vaadin-date-picker theme="small" placeholder="Fecha Inicio"></vaadin-date-picker>
                    </div>
                    <div class="dtfechafin">
                            <vaadin-date-picker theme="small" placeholder="Fecha Fin"></vaadin-date-picker>
                   </div>
                    <div class="cmbestado">
                        <vaadin-combo-box theme="small" placeholder="Estado"></vaadin-combo-box> 
                    </div>
                    <div class="btnbusqueda">
                        <iron-icon icon="vaadin:search" @click="${this.filtrarCotizaciones}"></iron-icon>
                    </div>
               </div>
        `;
    }
}

customElements.define('ctz-filtro-busqueda', CtzFiltroBusqueda);