import { LitElement, html } from 'lit-element';
import { connect } from 'pwa-helpers';
import { store } from '../redux/store.js';
import '../redux/reducer.js';
import { seleccionarItem, updateBusquedaCotizaciones } from '../redux/actions.js';
import * as Config from '../config/variable_config';
import axios from 'axios';
import { Router } from '@vaadin/router';

class CtzParrilla extends connect(store)(LitElement) {

    static get properties() { 
        return {
            busqueda: { type: Array },
            usuario:  {'nombre':String,'email':String,'token':String},
        };
    }

    constructor(){
        super();
    }

    stateChanged(state){
        this.usuario = state.user;
        this.busqueda = state.cotizaciones;
    }
    
    seleccionarEliminacionItem(item){
        console.log("Ingreso a seleccionarEliminacionCotizacion.");
        console.log(item);
        let i = 0;
        let cont = 0;
        let resultBusq = [];
        let filter='/idsol/'+item.id;
        let data;

        const reqHeaderToken = {
            headers: {'Authorization': 'JWT ' + this.usuario.token}
        };

        axios.delete(Config.URL_BACK + '/v1/cotizaciones' + filter, reqHeaderToken)
        .then(response => { 
            console.log("respuesta delete cotizacion");
            data = response.data.data;
            console.log(data);
            
            while (i<this.busqueda.length){
                if(item.id != this.busqueda[i].id){
                    resultBusq[cont] = this.busqueda[i];
                    cont++;
                }
                i++;
            }
            store.dispatch(updateBusquedaCotizaciones(resultBusq));            
        });
    }    

    seleccionarUpdateItem(item){
        console.log(item);
        console.log("Ingreso a seleccionarUpdateItem.");
        store.dispatch(seleccionarItem(item))
        Router.go('/evaluacion');
    }      

    render(){
        return html`

        <style>
            #grilla {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            #grilla td, #grilla th {
                border: 1px solid #ddd ;
                padding: 8px;
            }

            #grilla tr:nth-child(even){background-color: #f2f2f2;}

            #grilla tr:hover {background-color: #99BAE5;}

            #grilla th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: var(--shade-color);
                color: white;
            }
            .edicion {
                color: var(--shade-color);
                text-decoration: none;
            }
            .edicion:hover  {
                color: var(--shade-color);
                text-decoration: underline;
                cursor:pointer;
            } 
            .edicion:active {
                color: var(--base-color);
                text-decoration: none;
            } 
            .mobile{
                display:"";
            }    
        @media only screen and (max-width: 768px) {
            .mobile{
                display:none;
            }
        }        
        </style>

        <table id="grilla">
            <tr>
                <th style="width:5%">Id</th>
                <th style="width:14%" class="mobile">Fecha</th>
                <th style="width:13%">Documento</th>
                <th style="width:35%" class="mobile">Nombre</th>
                <th style="width:13%">Importe</th>
                <th style="width:5%" class="mobile">Tea Sol.</th>
                <th style="width:5%">Tea Aprob.</th>
                <th style="width:5%" class="mobile">Plazo</th>
                <th style="width:5%">Estado</th>
                <th class="mobile">Desemb.</th>
                <th>+</th>
                <th>-</th>
            </tr>
            ${this.busqueda.map(
                item => html` 
                    <tr>
                        <td>${item.id}</td>
                        <td class="mobile">${item.fecha.substring(0, 10)}</td>
                        <td>${item.documento}</td>
                        <td class="mobile">${item.nombre}</td>  
                        <td>${item.moneda} ${item.importe}</td>  
                        <td class="mobile">${item.teasol.length>6?item.teasol.substring(0,5):item.teasol}</td>  
                        <td>${item.teaprob.length>6?item.teaprob.substring(0,5):item.teaprob}</td>   
                        <td class="mobile">${item.unidad}-${item.plazo}</td>  
                        <td>${item.estado}</td>  
                        <td class="mobile">${item.desembolso}</td>   
                        <td><div class="edicion" @click="${ e => this.seleccionarUpdateItem(item)}">Editar</div></td>
                        <td><div class="edicion" @click="${ e => this.seleccionarEliminacionItem(item)}">Eliminar</div></td>                    
                    </tr>
            `)}            

        </table>
        `;
    }
}

customElements.define('ctz-parrilla', CtzParrilla);