import { LitElement, html } from 'lit-element';
import { connect } from 'pwa-helpers';
import { store } from '../redux/store.js';
import { logoutUser } from '../redux/actions.js';
import { Router } from '@vaadin/router';
import '../redux/reducer.js';
import sha256 from 'sha256';
import axios from 'axios';
import * as Config from '../config/variable_config';

class CtzHeader extends connect(store)(LitElement) {

  static get properties() { 
    return {
      view : { type: String },
      usuario:  {'nombre':String,'email':String,'token':String}
    };
  }

  stateChanged(state){
    try {
      this.usuario = state.user;
    } catch (error) {
      //console.log(error);
      document.location=Config.URL_BASE;
    } 
  }

  constructor(){
    super();
    this.view = "";    
  }

  cerrarSesion(){
    if(this.usuario.token == String){
        Router.go('/');
    } else {
      let hashToken = sha256(this.usuario.token);
      const reqHeaderToken = {
        headers: {'Authorization': 'JWT ' + this.usuario.token}
      };
      axios.delete(Config.URL_BACK + '/v1/auth/' + hashToken, reqHeaderToken)
      .then(response => { 
          console.log(response);
          store.dispatch(logoutUser(this.usuario)); 
          //Necesario para resetear el Redux o Componentes pegados.
          document.location=Config.URL_BASE;
          //Router.go('/');
      }).catch(e => {
          //console.log(e);
          document.location=Config.URL_BASE;
      });
    }
  }

  render() {
    return html`
        <style>
          a {
            color: var(--dark-color);
          }
          a:visited,
          a:active {
            color: var(--shade-color);
          }
          header,
          nav {
            padding: var(--spacing);
          }
          nav {
            height: 2em;
          }
          header {
            background: var(--dark-color);
            color: var(--base-color);
          }
          .profileUsuario{
            display:flex;
            flex-direction:row;
            text-align:right;
            font-size: 14px;
            justify-content:flex-end;
          }

          .profileUsuario .nombre{
            padding-top:5px;
            padding-left:5px;
          }

          .profileUsuario img{
            width:30px;
            height:30px;
          } 
          .tituloPrincipal{
            font-size: 24px;
            font-weight: bold;            
          }
          .profileCerraSesion{
            text-align:right;
            font-size: 14px;
          }
          .profileCerraSesion div {
            color: var(--base-color);
            text-decoration: none;
          }
          .profileCerraSesion div:hover  {
            color: var(--base-color);
            text-decoration: underline;
            cursor:pointer;
          }
          .profileCerraSesion div:active {
            color: var(--shade-color);
            text-decoration: none;
          }
        </style>
        <header>
          <div class="profileUsuario">
            <img src="https://s3-sa-east-1.amazonaws.com/cotizador-web/imagenes/usuario.png">
            <div class="nombre">
            ${this.usuario.nombre}
            </div>
          </div>
          <div class="tituloPrincipal">
            Cotizador Web
          </div>
          <div class="profileCerraSesion">
            <div @click="${this.cerrarSesion}">cerrar sesion</div>
          </div>
        </header>
        <nav> 
            ${this.view==='bandeja'?
                html`<a>Bandeja</a>`:
                html`<a href="/bandeja">Bandeja</a>`}           
            ${this.view==='cotizador'?
                html`<a>Cotizador</a>`:
                html`<a href="/cotizador">Cotizador</a>`}    
            <a href="/stats">Stats</a>
        </nav>
    `;
  }
}

customElements.define('ctz-header', CtzHeader);