import { LitElement, html } from 'lit-element';
import { connect } from 'pwa-helpers';
import { store } from '../redux/store.js';
import { updateCotizacion } from '../redux/actions.js';
import '../redux/reducer.js';

class CtzDatosProducto extends connect(store)(LitElement) {

  static get properties() { 
    return {
        cotizacion:{type:Object},        
 
        plazo:{tipoPeriodo:String,periodo:String},
        importeOperacion:{moneda:String,monto:String},
        producto:{tipoProducto:String,nomProducto:String},
        modalidad:{type:String},
        garantia:{type:String},

        productos: {type:Array},
        tipoproductos: {type:Array},
        monedas: {type:Array},
        modalidades:{type:Array},
        garantias:{type:Array},
        periodos:{type:Array},
        accion:{type:String}        
    };
  }
    constructor(){
        super();
        this.productos = ['PLD', 'VEH'];
        this.tipoproductos = ['PLD', 'VEH'];
        this.monedas = ['PEN', 'USD'];
        this.modalidades = ['Nuevo'];
        this.garantias = ['Sin Garantia'];
        this.periodos = ['MES', 'ANUAL'];
        this.limpiarEmpty();
    }
    disconnectedCallback() {
        this.limpiarEmpty();
        super.disconnectedCallback();
      }    

    stateChanged(state){
        this.cotizacion = state.cotizacion;
        this.plazo={tipoPeriodo:state.cotizacion.plazo.tipoPeriodo,periodo:(state.cotizacion.plazo.periodo===String?"":state.cotizacion.plazo.periodo)};
        this.importeOperacion={moneda:state.cotizacion.importeOperacion.moneda,monto:(state.cotizacion.importeOperacion.monto===String?"":state.cotizacion.importeOperacion.monto)};
        this.producto={tipoProducto:state.cotizacion.producto.tipoProducto,nomProducto:state.cotizacion.producto.nomProducto};
        this.modalidad=""+state.cotizacion.modalidad; 
        this.garantia=""+state.cotizacion.garantia;         
    }

    limpiarEmpty(){
        this.plazo={tipoPeriodo:"",periodo:""};
        this.importeOperacion={moneda:"",monto:""};
        this.producto={tipoProducto:"",nomProducto:""};
        this.modalidad=""; 
        this.garantia="";    
    }

    updateMoneda(e){
        this.cotizacion.importeOperacion.moneda=e.target.value;
        store.dispatch(updateCotizacion(this.cotizacion));
    }

    updateImporte(e){
        this.cotizacion.importeOperacion.monto=e.target.value;
        store.dispatch(updateCotizacion(this.cotizacion));
    }

    updatePlazo(e){
        this.cotizacion.plazo.periodo=e.target.value;
        store.dispatch(updateCotizacion(this.cotizacion));
    }

    updateProducto(e){
        this.cotizacion.producto.nomProducto=e.target.value;
        store.dispatch(updateCotizacion(this.cotizacion));
    }   
    
    updateTipoProducto(e){
        this.cotizacion.producto.tipoProducto=e.target.value;
        store.dispatch(updateCotizacion(this.cotizacion));
    }    
    
    updatePeriodo(e){
        this.cotizacion.plazo.tipoPeriodo=e.target.value;
        store.dispatch(updateCotizacion(this.cotizacion));
    }  
 
    updateGarantia(e){
        this.cotizacion.garantia=e.target.value;
        store.dispatch(updateCotizacion(this.cotizacion));
    }   
    
    updateModalidad(e){
        this.cotizacion.modalidad=e.target.value;
        store.dispatch(updateCotizacion(this.cotizacion));
    }     

  render() {
    return html`
        <style>
            .contenedorDatosProducto {
                display: grid;
                
                grid-template-columns: 1fr 1fr 1fr;
                grid-template-rows: auto;
                grid-template-areas: "dato-producto dato-tipo-producto dato-Modalidad"
                                     "dato-importe dato-importe dato-garantia"
                                     "dato-plazo dato-plazo dato-plazo";
                column-gap: 5px;
                row-gap: 0px;
                margin-bottom: 0px;
                width: 450px;
                padding: 0px;
            }
            .divProducto {
                grid-area: dato-producto;
                display: flex;
                flex-direction:column;
                align-items: flex-start;
            }
            .divProducto vaadin-combo-box {
                width:140px;
            }
            
            .divTipoProducto {
                grid-area: dato-tipo-producto;
                display: flex;
                flex-direction:column;
                align-items: flex-start;
            }
            .divTipoProducto vaadin-combo-box {
                width:140px;
            } 
            
            .divModalidad {
                grid-area: dato-Modalidad;
                display: flex;
                flex-direction:column;
                align-items: flex-start;
            }
            .divModalidad vaadin-combo-box {
                width:145px;
            }
            
            .divImporte {
                grid-area: dato-importe;
                display: flex;
                flex-direction: column;
                align-items: flex-start;               
            }
            #datoImporte{
                display:flex;
                flex-direction:row;
            }
            #divMoneda {
                margin-right:5px;
            }  
            #divMoneda vaadin-combo-box {
                width:100px;
            } 
            #divMonto vaadin-text-field {
                width:140px;
            } 

            .divGarantia {
                grid-area: dato-garantia;
                display: flex;
                flex-direction:column;
                align-items: flex-start;
            }
            .divGarantia vaadin-combo-box {
                width:145px;
            } 
            
            .divPlazo {
                grid-area: dato-plazo;
                display: flex;
                flex-direction: column;
                align-items: flex-start;               
            }
            #datoPlazo{
                display:flex;
                flex-direction:row;
            }
            #divNomMes {
                margin-right:5px;
            }  
            #divNomMes vaadin-combo-box {
                width:100px;
            } 
            #divNumMes vaadin-text-field {
                width:140px;
            }             
        </style> 
        <div class="contenedorDatosProducto">
            <div class="divProducto">
                <div>Producto:</div>
                <div>
                    <vaadin-combo-box id="cmbProducto" theme="small" class="custom-style" value=${this.cotizacion.producto.nomProducto} items='${JSON.stringify(this.productos)}' @change="${this.updateProducto}">                       
                    </vaadin-combo-box> 
                </div>  
            </div>
            <div class="divTipoProducto">
                <div>Tipo de Producto:</div>
                <div>
                    <vaadin-combo-box id="cmbTipoProducto" theme="small" class="custom-style" value=${this.cotizacion.producto.tipoProducto} items='${JSON.stringify(this.tipoproductos)}' @change="${this.updateTipoProducto}">                        
                    </vaadin-combo-box> 
                </div>  
            </div>
            <div class="divModalidad">
                <div>Modalidad:</div>
                <div>
                    <vaadin-combo-box id="cmbModalidad" theme="small" class="custom-style" placeholder="" value=${this.modalidad} items='${JSON.stringify(this.modalidades)}' @change="${this.updateModalidad}">                        
                    </vaadin-combo-box> 
                </div>  
            </div>
            <div class="divImporte">
                <div>Importe de la Operación:</div>
                <div id="datoImporte">
                    <div id="divMoneda">
                        <vaadin-combo-box id="cmbMoneda" theme="small" class="custom-style" label="" value=${this.importeOperacion.moneda} items='${JSON.stringify(this.monedas)}' @change="${this.updateMoneda}">
                        </vaadin-combo-box>
                    </div>
                    <div id="divMonto">
                        <vaadin-text-field id="txtMonto" theme="small" class="custom-style" value=${this.importeOperacion.monto} placeholder="" @change="${this.updateImporte}"> 
                        </vaadin-text-field>
                    </div>
                </div>
            </div>
            <div class="divGarantia">
                <div>Garantía:</div>
                <div>
                    <vaadin-combo-box id="cmbGarantia" theme="small" class="custom-style" placeholder="" value=${this.garantia} items='${JSON.stringify(this.garantias)}' @change="${this.updateGarantia}">                        
                    </vaadin-combo-box> 
                </div>  
            </div>
            <div class="divPlazo">
                <div>Plazo:</div>
                <div id="datoPlazo">
                    <div id="divNomMes">
                        <vaadin-combo-box id="cmbMes" theme="small" class="custom-style" placeholder="" value=${this.plazo.tipoPeriodo} items='${JSON.stringify(this.periodos)}' @change="${this.updatePeriodo}">
                        </vaadin-combo-box> 
                    </div>
                    <div id="divNumMes">
                        <vaadin-text-field id="txtNumMes" theme="small" class="custom-style" placeholder="" value=${this.plazo.periodo} @change="${this.updatePlazo}"> 
                        </vaadin-text-field>
                    </div>                     
                </div>
            </div>  
        </div>           
        `;
  }
}

customElements.define('ctz-datos-producto', CtzDatosProducto);        