import { LitElement, html } from 'lit-element';
import { connect } from 'pwa-helpers';
import { store } from '../redux/store.js';
import { updatePersona, updateTea, updateCotizacion } from '../redux/actions.js';
import '../redux/reducer.js';
import axios from 'axios';
import * as Config from '../config/variable_config';
import { Router } from '@vaadin/router';

class CtzCalculadoraPrestamo extends connect(store)(LitElement) {

    static get properties() { 
        return {
            persona:{type:Object},
            cotizacion:{type:Object},
            usuario:{type:Object},
            tea:{type:Object},
            comentarios:{type:String},
            estado:{type:String},
            teaAprob:{type:String},
            estados:{type:Array},
            accion:{type:String}
        };
    }
    constructor(){
        super();
        this.estados = ['SOLICITADA', 'APROBADA'];
        this.limpiarEmpty();      
    }

    stateChanged(state){
        this.usuario = state.user;
        this.persona = state.persona;
        this.cotizacion = state.cotizacion;
    }

    limpiarEmpty(){
        this.comentarios="";
        this.tea={teaMinima:"", teaSugerida:"", teaSolicitada:""};        
    }

    calcularTasaModelo(){
        var ruleKey = "";
        const reqHeaderToken = {
            headers: {'Authorization': 'JWT ' + this.usuario.token}
        };
        axios.get(Config.URL_BACK + '/v1/rules/plazo/'+ this.cotizacion.plazo.periodo+'/monto/'+this.cotizacion.importeOperacion.monto, reqHeaderToken)
        .then(response => { 
            console.log("Obteniendo respuesta Rule JSON");
            console.log(response.data); 
            ruleKey=response.data.data.key;
            let reqHeader = { 
                headers: {'Content-Type':'application/json'} 
            };
            console.log("Ingreso calcularTasaModelo: " + ruleKey);
            axios.get(Config.URL_TEA + ruleKey,reqHeader)
                .then(response => { 
                    console.log("Obteniendo respuesta Mocky");
                    console.log(response.data);
                    var teaMinima=parseFloat(response.data.data[0].effectiveRate.detailedEffectiveRates[0].percentage)*100;
                    var teasugerida=parseFloat(response.data.data[0].effectiveRate.detailedEffectiveRates[1].percentage)*100;
                    this.tea = {'teaMinima':teaMinima,'teaSugerida':teasugerida,'teaSolicitada':teasugerida};
                    //store.dispatch(updateTea({'teaMinima':teaMinima,'teaSugerida':teasugerida,'teaSolicitada':teasugerida}));  
                }).catch(e => {
                    console.log(e);
                });
        }).catch(e => {
            console.log(e);
        });
    }

    registrarCotizacion(){
        console.log("Ingreso registrarCotizacion");
        console.log(this.cotizacion);
        //debugger;
        const reqHeaderToken = {
            headers: {'Authorization': 'JWT ' + this.usuario.token}
        };
        axios.post(Config.URL_BACK + '/v1/cotizaciones', {
            codPersona:this.persona.codCentral,
            documento:{tipoDocumento:this.persona.tipoDocumento,numDocumento:this.persona.numDocumento},
            nomPersona:this.persona.nombre,
            producto:{tipoProducto:this.cotizacion.producto.tipoProducto,nomProducto:this.cotizacion.producto.nomProducto},
            importeOperacion:{moneda:this.cotizacion.importeOperacion.moneda,monto:this.cotizacion.importeOperacion.monto},
            plazo:{tipoPeriodo:this.cotizacion.plazo.tipoPeriodo,periodo:this.cotizacion.plazo.periodo},
            modalidad:this.cotizacion.modalidad,
            garantia:this.cotizacion.garantia,
            teaSugerida:this.tea.teaSugerida,
            teaMinima:this.tea.teaMinima,
            teaSolicitada:this.tea.teaSolicitada,
            comentarios:this.comentarios,
            estado:'SOLICITADA'
        },reqHeaderToken)
        .then(response => { 
            alert("Cotizacion Registrada");
            console.log("Obteniendo respuesta POST cotizacion");
            console.log(response.data); 
            this.resetCotizacion();
        }).catch(e => {
            console.log(e);
        }); 
    }
    
    resetCotizacion(){
        store.dispatch(updatePersona({tipoDocumento:"",numDocumento:"",codCentral:"",nombre:"",buro:"",fecAlta:"",nivelRiesgo:"",
                vinculacion:"",segmento:{codSegmento:"",nomSegmento:""},oficina:{codOficina:"",nomOficina:""},
                gestor:{codGestor:"",nomGestor:""}}));
        
        store.dispatch(updateCotizacion({producto:{tipoProducto:"",nomProducto:""},importeOperacion:{moneda:"",monto:""},
                plazo:{tipoPeriodo:"",periodo:""},modalidad:"",garantia:"",teaSugerida:"",
                teaMinima:"",teaSolicitada:"",comentarios:"",estado:""}));
        
        this.limpiarEmpty()
    } 

    updateComentarios(e){
        this.comentarios=e.target.value;
    }  

    updateEstado(e){
        this.estado=e.target.value;
    }
    
    updateTeaApro(e){
        this.tea.teaMinima=e.target.value;
    }

    cancelar(){
        this.resetCotizacion();
        Router.go('/bandeja');  
    }

    render() {
        return html`
            <style>
                .contenedorCalculadoraPrestamo {
                    display: grid;
                    
                    grid-template-columns: 1fr 1fr 1fr;
                    grid-template-rows: auto;
                    grid-template-areas: "dato-tea-sugerida dato-tea-minima btnCalcular"
                                            "dato-tea-solicitada dato-tea-solicitada dato-tea-solicitada"
                                            "dato-tea-sustento dato-tea-sustento dato-tea-sustento"
                                            "btnRegistrar btnRegistrar btnRegistrar";
                    column-gap: 5px;
                    row-gap: 0px;
                    margin-bottom: 0px;
                    width: 450px;
                    padding: 0px;
                }
                .divTeaSugerida {
                    grid-area: dato-tea-sugerida;
                    display: flex;
                    flex-direction: column;
                    align-items: flex-start;               
                }                      
                .divTeaSugerida vaadin-text-field {
                    width:120px;
                }

                .divTeaMinima {
                    grid-area: dato-tea-minima;
                    display: flex;
                    flex-direction: column;
                    align-items: flex-start;               
                }                      
                .divTeaMinima vaadin-text-field {
                    width:120px;
                }   
                
                .divCalcular {
                    grid-area: btnCalcular;
                    display: flex;
                    flex-direction: column-reverse;
                    align-items: flex-end;               
                } 
                .divCalcular vaadin-button {
                    width: 140px;
                }                
                
                .divTeaSolicitada {
                    grid-area: dato-tea-solicitada;
                    display: flex;
                    flex-direction: column;
                    align-items: flex-start;               
                }                      
                .divTeaSolicitada vaadin-text-field {
                    width:120px;
                }   
                
                .divSustento {
                    grid-area: dato-tea-sustento;
                    display: flex;
                    flex-direction: column;
                    align-items: flex-start;               
                }                      
                .divSustento vaadin-text-field {
                    width:448px;
                }

                .divRegistrar {
                    grid-area: btnRegistrar;
                    display: flex;
                    flex-direction: row-reverse;
                    align-items: flex-end;               
                } 
                .divRegistrar vaadin-button {
                    width: 140px;
                }           
            </style> 
            <div class="contenedorCalculadoraPrestamo">
                <div class="divTeaSugerida">
                ${this.accion==='nuevo'?
                    html`                
                    <div>TEA Sugerida:</div>
                    <div>
                        <vaadin-text-field id="txtTeaSugerida" theme="small" class="custom-style" placeholder="" value=${this.tea.teaSugerida}> 
                        </vaadin-text-field>
                    </div>`
                    :html`<div>TEA Aprobada:</div>
                            <div>
                                <vaadin-text-field id="txtTeaAprobada" theme="small" class="custom-style" placeholder="" value=${this.teaAprob} @change="${this.updateTeaApro}"> 
                                </vaadin-text-field>
                            </div>`
                }                     
                </div>
                <div class="divTeaMinima">
                ${this.accion==='nuevo'?
                        html`
                            <div>TEA Mínima:</div>
                            <div>
                                <vaadin-text-field id="txtTeaMinima" theme="small" class="custom-style" placeholder="" value=${this.tea.teaMinima}>
                                </vaadin-text-field>
                            </div>`
                        :html``
                }         
                </div>
                <div class="divCalcular">
                    <div>
                    ${this.accion==='nuevo'?
                        html`<vaadin-button theme="small" @click="${this.calcularTasaModelo}">Calcular Tasa</vaadin-button>`
                        :html``
                    }                      
                    </div>
                </div>
                <div class="divTeaSolicitada">
                    ${this.accion==='nuevo'?
                        html`<div>TEA Solicitada:</div>
                            <div>
                                <vaadin-text-field id="txtTeaSolicitada" theme="small" class="custom-style" placeholder="" value=${this.tea.teaSolicitada}> 
                                </vaadin-text-field>
                            </div>`
                        :html`<div>Estado:</div>
                            <div>
                                <vaadin-combo-box id="cmbEstado" theme="small" class="custom-style" placeholder="" value=${this.estado} items='${JSON.stringify(this.estados)}' @change="${this.updateEstado}">                        
                                </vaadin-combo-box> 
                            </div>`
                    }
                </div>
                <div class="divSustento">
                ${this.accion==='nuevo'?
                    html`                
                    <div>Indicar sustento correspondiente:</div>
                    <div>
                            <vaadin-text-field id="txtTeaSustento" theme="small" class="custom-style" placeholder="" .value=${this.comentarios} @change="${this.updateComentarios}"> 
                             </vaadin-text-field>
                        
                    </div>`
                    :html``
                    }                    
                </div>
                <div class="divRegistrar">
                    <div> 
                    ${this.accion==='nuevo'?
                        html`<vaadin-button theme="success small" @click="${this.registrarCotizacion}">Registrar</vaadin-button>`
                        :html`<vaadin-button theme="success small" @click="${this.registrarCotizacion}">Actualizar</vaadin-button>
                              <br>
                              <vaadin-button theme="success small" @click="${this.cancelar}">Cancelar</vaadin-button>
                              <br>`
                    }                             
                        
                    </div>
                </div>      
            </div>             
            `;
    }
}

customElements.define('ctz-calculadora-prestamo', CtzCalculadoraPrestamo);        