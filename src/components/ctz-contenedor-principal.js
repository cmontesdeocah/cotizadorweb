import { LitElement, html } from 'lit-element';

class CtzContenedorPrincipal extends LitElement {

    render(){
     return html`
        <style>
            .marco {
                padding-left:16px;
                padding-right:16px;
            }
        </style>
        <div class="marco">
            <slot></slot>
        </div>
     `; 
    }
}

customElements.define('ctz-contenedor-principal', CtzContenedorPrincipal);