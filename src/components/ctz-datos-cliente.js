import { LitElement, html } from 'lit-element';
import { connect } from 'pwa-helpers';
import { store } from '../redux/store.js';
import { updatePersona, updateTea } from '../redux/actions.js';
import '../redux/reducer.js';
import axios from 'axios';
import * as Config from '../config/variable_config';

class CtzDatosCliente extends connect(store)(LitElement) {

    static get properties() { 
        return {
            persona:{tipoDocumento:String,numDocumento:String,codCentral:String,nombre:String,buro:String,fecAlta:String,nivelRiesgo:String,
                vinculacion:String,segmento:{codSegmento:String,nomSegmento:String},oficina:{codOficina:String,nomOficina:String},
                gestor:{codGestor:String,nomGestor:String}},
            tea:{teaMinima:String,teaSugerida:String, teaSolicitada:String},
            usuario:  {'nombre':String,'email':String,'token':String},
            documentos: {type:Object},
            tipoDocumento: { type:String },
            numDocumento: { type:String },
            accion: {type:String}
        };
    }

    stateChanged(state){
        this.usuario = state.user;
        this.persona = state.persona;
        this.tea = state.tea;
        this.numDocumento="" + state.persona.numDocumento;
    }

    constructor(){
        super();
        this.view = "";
        this.documentos = ['DNI', 'EXT'];
        this.tipoDocumento="DNI";
        this.numDocumento="";
        this.resetPersona();
        
    }

    updateNumDocumento(e){
        this.numDocumento = e.target.value;
    } 

    resetPersona(){
        store.dispatch(updatePersona({tipoDocumento:"",numDocumento:"",codCentral:"",nombre:"",buro:"",fecAlta:"",nivelRiesgo:"",
                vinculacion:"",segmento:{codSegmento:"",nomSegmento:""},oficina:{codOficina:"",nomOficina:""},
                gestor:{codGestor:"",nomGestor:""}}));
        store.dispatch(updateTea({teMinima:"",teaSugerida:"",teaSolicitada:""}));
        this.tipoDocumento="DNI";
        this.numDocumento="";
    }    

    obtenerPersona(){
        console.log("Ingreso obtener Persona");
        const reqHeaderToken = {
            headers: {'Authorization': 'JWT ' + this.usuario.token}
        };
        axios.get(Config.URL_BACK + '/v1/personas/tipoDocumento/'+ this.tipoDocumento+'/numDocumento/'+this.numDocumento, reqHeaderToken)
        .then(response => { 
            console.log("Obteniendo respuesta Persona JSON");
            console.log(response.data); 
            store.dispatch(updatePersona({'tipoDocumento':this.tipoDocumento,'numDocumento':this.numDocumento,'codCentral':response.data.data.codCentral,
                                        'nombre':response.data.data.nombre,'buro':response.data.data.buro,'fecAlta':response.data.data.fecAlta,
                                        'nivelRiesgo':response.data.data.nivelRiesgo,'vinculacion':response.data.data.vinculacion,'segmento':response.data.data.segmento,
                                        'oficina':response.data.data.oficina,'gestor':response.data.data.gestor}));
              
        }).catch(e => {
            this.resetPersona();
            console.log(e);
        });          
    }    

    render() {
        return html`
            <style>

                .contenedorDatosCliente {
                    display: grid;
                    
                    grid-template-columns: 1fr 1fr 1fr 1fr;
                    grid-template-rows: auto;
                    grid-template-areas: "txt-codcentral cmb-tpdoc txt-numdoc btn-buscar"
                                            "txt-nombre txt-nombre txt-nombre txt-nombre";
                    column-gap: 5px;
                    row-gap: 0px;
                    margin-bottom: 0px;
                    width: 450px;
                    padding: 0px;
                }
                .divCodigoCentral {
                    grid-area: txt-codcentral;
                    display: flex;
                    align-items: center;
                }
                .divTipoDocumento {
                    grid-area: cmb-tpdoc;
                    display: flex;
                    align-items: center;
                }            
                .divNumeroDocumento {
                    grid-area: txt-numdoc;
                    display: flex;
                    align-items: center;                
                }
                .divNombre {
                    grid-area: txt-nombre;
                    display: flex;
                    align-items: center;
                }
                .divBuscar {
                    grid-area: btn-buscar;
                    display: flex;
                    align-items: center;
                    font-weight:500;               
                }

                            
                .divCodigoCentral vaadin-text-field {
                    width:130px;
                }
                .divTipoDocumento vaadin-combo-box  {
                    width:100px;
                }
                .divNumeroDocumento vaadin-text-field {
                    width:130px;
                }            
                .divNombre vaadin-text-field {
                    width:370px;
                }
            
            </style> 
            <div class="contenedorDatosCliente">
                <div class="divCodigoCentral">
                        <vaadin-text-field id="txtCodigoCentral" theme="small" class="custom-style" placeholder="Código Central" value=${this.persona.codCentral}> 
                        </vaadin-text-field>
                </div>
                <div class="divTipoDocumento">
                    <vaadin-combo-box id="cmbTipoDocumento" theme="small" class="custom-style" placeholder="T.Doc" value=${this.tipoDocumento} items='${JSON.stringify(this.documentos)}'>
                    </vaadin-combo-box> 
                </div>
                <div class="divNumeroDocumento">
                        <vaadin-text-field id="txtNumeroDocumento" theme="small" class="custom-style" placeholder="Nro. Documento" value=${this.numDocumento} @change="${this.updateNumDocumento}"> 
                        </vaadin-text-field>
                </div>
                <div class="divNombre">              
                        <vaadin-text-field id="txtNombreCliente" theme="small" class="custom-style" placeholder="Nombre cliente" value=${this.persona.nombre}> 
                        </vaadin-text-field>
                </div>
                <div class="divBuscar">
                ${this.accion==='nuevo'?
                        html`<vaadin-button theme="small" @click="${this.obtenerPersona}">Buscar</vaadin-button>`
                        :html``
                 }       
                </div>     
            </div>             
            `;
    }
}

customElements.define('ctz-datos-cliente', CtzDatosCliente);        