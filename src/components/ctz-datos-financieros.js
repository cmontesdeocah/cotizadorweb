import { LitElement, html } from 'lit-element';
import { connect } from 'pwa-helpers';
import { store } from '../redux/store.js';
import '../redux/reducer.js';

class CtzDatosFinancieros extends connect(store)(LitElement) {

    static get properties() {
        return {
            persona:{tipoDocumento:String,numDocumento:String,codCentral:String,nombre:String,buro:String,fecAlta:String,nivelRiesgo:String,
                vinculacion:String,segmento:{codSegmento:String,nomSegmento:String},oficina:{codOficina:String,nomOficina:String},
                gestor:{codGestor:String,nomGestor:String}},
            accion:{type:String}
        };
    }

    stateChanged(state) {
        this.persona = state.persona;
    }

    constructor() {
        super();
        this.view = "";
        this.documentos = ['DNI', 'EXT'];
    }

    render() {
        return html`
        <style>

            .contenedorDatosFinancieros {
                display: grid;
                
                grid-template-columns: 1fr 1fr 1fr 1fr;
                grid-template-rows: auto;
                grid-template-areas:    "dato-segmento dato-segmento dato-buro dato-alta"
                                        "dato-riesgo dato-riesgo dato-riesgo dato-riesgo"
                                        "dato-oficina-principal dato-oficina-principal dato-gestor dato-gestor"
                                        "dato-vinculacion dato-vinculacion dato-vinculacion dato-vinculacion";
                column-gap: 5px;
                row-gap: 0px;
                margin-bottom: 0px;
                width: 450px;
                padding: 0px;
            }

            .divSegmento {
                grid-area: dato-segmento;
                display: flex;
                flex-direction: column;
                align-items: flex-start;    
            }
            #datoSegmento{
                display:flex;
                flex-direction:row;
            }
            #divCodSegmento{
                margin-right:5px;
            }           
            #divCodSegmento vaadin-text-field {
                width:80px;
            }
            #divNomSegmento vaadin-text-field {
                width:130px;
            } 

            .divBuro {
                grid-area: dato-buro;
                display: flex;
                flex-direction: column;
            } 
            .divBuro vaadin-text-field {
                width:120px;
            }

            .divAlta {
                grid-area: dato-alta;                
            }
            .divAlta vaadin-text-field {
                width:100px;
            }

            .divRiesgo {
                grid-area: dato-riesgo;
            }
            .divRiesgo vaadin-text-field {
                width:445px;
            }

            .divOficinaPrincipal {
                grid-area: dato-oficina-principal;
                display: flex; 
                flex-direction: column;
                align-items: flex-start;               
            }
            #datoOficina {
                display:flex;
                flex-direction:row;
            }
            #divCodOficina {
                margin-right:5px;
            }             
            #divCodOficina vaadin-text-field {
                width:80px;
            } 
            #divNomOficina vaadin-text-field {
                width:130px;
            }             

            .divGestor {
                grid-area: dato-gestor;
                display: flex;
                flex-direction: column;
                align-items: flex-start;               
            }
            #datoGestor{
                display:flex;
                flex-direction:row;
            }
            #divCodGestor {
                margin-right:5px;
            }  
            #divCodGestor vaadin-text-field {
                width:80px;
            } 
            #divNomGestor vaadin-text-field {
                width:140px;
            }               

            .divVinculacion {
                grid-area: dato-vinculacion;
                display: flex;
                flex-direction: column;
                align-items: flex-start;               
            }                      
            .divVinculacion vaadin-text-field {
                width:215px;
            }                 
        
        </style> 
        <div class="contenedorDatosFinancieros">
            <div class="divSegmento">
                <div id="lblSegmento">Segmento:</div>
                <div id="datoSegmento">
                    <div id="divCodSegmento">
                        <vaadin-text-field id="txtCodigoSegmento" placeholder="" theme="small" class="custom-style" value=${this.persona.segmento.codSegmento}> 
                        </vaadin-text-field>
                    </div>
                    <div id="divNomSegmento">
                        <vaadin-text-field id="txtNombreSegmento" placeholder="" theme="small" class="custom-style" value=${this.persona.segmento.nomSegmento}> 
                        </vaadin-text-field>
                    </div>
                </div>                
            </div>
            <div class="divBuro">
                <div>Buró:</div>
                <div>
                    <vaadin-text-field id="txtCodBuro" placeholder="" theme="small" class="custom-style" value=${this.persona.buro}> 
                    </vaadin-text-field>
                </div>
            </div>
            <div class="divAlta">
                <div>Fecha Alta:</div>
                <div>
                    <vaadin-text-field id="txtFecAlta" placeholder="" theme="small" class="custom-style" value=${this.persona.fecAlta}> 
                    </vaadin-text-field>
                </div>
            </div>
            <div class="divRiesgo">
                <div>Nivel Riesgo:</div>
                <div>
                    <vaadin-text-field id="txtNivelRiesgo" placeholder="" theme="small" class="custom-style" value=${this.persona.nivelRiesgo}> 
                    </vaadin-text-field>
                </div>
            </div>
            
            <div class="divOficinaPrincipal">
                <div>Oficina Principal:</div>
                <div id="datoOficina">
                    <div id="divCodOficina">
                        <vaadin-text-field id="txtCodOficina" placeholder="" theme="small" class="custom-style" value=${this.persona.oficina.codOficina}> 
                        </vaadin-text-field>
                    </div>
                    <div id="divNomOficina">
                        <vaadin-text-field id="txtNombreOficina" placeholder="" theme="small" class="custom-style" value=${this.persona.oficina.nomOficina}> 
                        </vaadin-text-field>
                    </div>
                </div>                
            </div>
            <div class="divGestor">
                <div>Gestor Principal:</div>
                <div id="datoGestor">
                    <div id="divCodGestor">
                        <vaadin-text-field id="txtCodGestor" placeholder="" theme="small" class="custom-style" value=${this.persona.gestor.codGestor}> 
                        </vaadin-text-field>
                    </div>
                    <div id="divNomGestor">
                        <vaadin-text-field id="txtNombreGestor" placeholder="" theme="small" class="custom-style" value=${this.persona.gestor.nomGestor}> 
                        </vaadin-text-field>
                    </div>
                </div>           
            </div>
            <div class="divVinculacion">
                <div>Vinculación:</div>
                <div>
                    <vaadin-text-field id="txtVinculacion" placeholder="" theme="small" class="custom-style" value=${this.persona.vinculacion}> 
                    </vaadin-text-field>
                </div>
            </div>  
        </div>             
        `;
    }
}

customElements.define('ctz-datos-financieros', CtzDatosFinancieros);        