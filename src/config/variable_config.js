const URL_BACK = "http://127.0.0.1:3000";
const URL_TEA = "http://www.mocky.io/v2/";
const PARAM_HEADER_AUTH = "Authorization";
const URL_BASE = location.protocol+"//"+window.location.hostname+":"+window.location.port+"/";
export { URL_BACK, URL_TEA, PARAM_AUTH, URL_BASE };